import cv2
import numpy as np
import pytesseract
import io
import os
import json
import codecs
import sys
import re
from pytesseract import pytesseract as pt
from natsort import natsort

from matplotlib import pyplot as plt
from PIL import Image

vBookTitle = ''
chaptersArray = [[]]
verseArray = []
singleChapter=[]

class resources:
    kazL = ['0x410', '0x4d8', '0x411', '0x412', '0x413', '0x492', '0x414', '0x415', '0x401', '0x416', '0x417', '0x418', 
    '0x419', '0x41a', '0x49a', '0x41b', '0x41c', '0x41d', '0x4a2', '0x41e', '0x4e8', '0x41f', '0x420', '0x421', '0x422', '0x423', '0x4b0', '0x4ae', '0x424', 
    '0x425', '0x4ba', '0x426', '0x427', '0x428', '0x429', '0x42a', '0x42b', '0x406', '0x42c', '0x42d', '0x42e', '0x42f']

#clean subtitle/verse for leading garbage
def cleanText(inputString):
    inputString=inputString.strip()
    if (inputString==''):
        return ''
    rsrcs = resources()

    iffound='N'
    while (inputString.strip()!='' and iffound=='N'):
        a=0
        for a in xrange(len(rsrcs.kazL)):
            if (inputString.strip()!='' and hex(ord(inputString[0]))== rsrcs.kazL[a]):
                iffound='Y'
                break
        if (iffound=='Y'):
            break
        else:
            inputString = inputString[1:len(inputString)]
    return inputString

def only_numerics(inputString):
    return re.sub('[^0-9]','', inputString)

def find_nth(haystack, needle, n):
    start = haystack.find(needle)
    while start >= 0 and n > 1:
        start = haystack.find(needle, start+len(needle))
        n -= 1
    return start

def get_chapt_num(filename):
    chaptName = ''
    if filename.count('-')>=2:
        firstHyphen = find_nth(filename, '-', 1)
        secHyphen = find_nth(filename, '-', 2)
        chaptName = filename[firstHyphen+1:secHyphen]
    return chaptName


def processBooks(booksDirLoc, toRemoveNewLine):

    #list folders of books in main dir
    bookDirs = next(os.walk(booksDirLoc))[1]

    for bookNumber in bookDirs:
        
        #booktitle
        print "Extracting book name..."
        bookRoot = booksDirLoc + "/" + bookNumber
        bookNameSliceLoc = bookRoot + "/bookname.jpg"
        bookName = str(bookNumber)
        if os.path.exists(bookNameSliceLoc)==True:
            inputImage = cv2.imread(bookNameSliceLoc, 1)
            ExtractedText = pytesseract.image_to_string(Image.fromarray(inputImage), lang="kaz")
            ExtractedText = cleanText(ExtractedText).strip()
            bookName = ExtractedText
        print "Book Name: " + bookName

        #list file names in a book folder
        vsFilesLoc = booksDirLoc + "/" + bookNumber  
        OriginalNameList = os.listdir(vsFilesLoc)
        #list naturally - lol!
        OriginalNameList = natsort(OriginalNameList)

        #list chapters
        chaptNumList = []
        for fname in OriginalNameList:
            cname = get_chapt_num(fname)
            if cname!='' and not cname in chaptNumList:
                #print "Chapter: " + cname
                chaptNumList.append(cname)
        print "Total Chapters: " + str(len(chaptNumList))

        #iterate to process now
        chaptersArray = [[]]
        TotalTakenSoFar = 0
        for cn in chaptNumList:
            print "Processing Chapter: " + str(chaptNumList.index(cn)+1) + "..."
            verseArray = []
            for fname in OriginalNameList:
                tfname = fname.replace(".jpg", "").strip()
                cnx = get_chapt_num(fname)
                if cnx == cn and not (fname.count('-')==3 and tfname.endswith("-2")):
                    imLoc = bookRoot+"/"+fname
                    if os.path.exists(imLoc)==True:
                        soV = "verse"

                        tSeg = 0
                        fSeg = 0
                        if fname.count('-')==3:
                            sPos = find_nth(tfname, '-', 2)+1
                            ePos = find_nth(tfname, '-', 3)
                            tSeg = int(tfname[sPos:ePos])

                            sPos = find_nth(tfname, '-', 3)+1
                            ePos = len(tfname)
                            fSeg = int(tfname[sPos:ePos])

                        if tSeg+1 == fSeg or tfname.endswith("-0-1") and not (tSeg==1 and fSeg== 2) :
                            soV = "subtitle"

                        #initial verse/substitle
                        inputImage = cv2.imread(imLoc, 1)
                        ExtractedText = pytesseract.image_to_string(Image.fromarray(inputImage), lang="kaz")
                        ExtractedText = cleanText(ExtractedText).strip()
                        TotalTakenSoFar+=1
                        
                        #fragmented verse
                        if fname.count('-')==3 and tfname.endswith('-1') and soV=='verse': 
                            nextFileName = OriginalNameList[OriginalNameList.index(fname)+1]
                            imLoc2 = bookRoot + "/" + nextFileName
                            if os.path.exists(imLoc2)==True:
                                inputImage = cv2.imread(imLoc2, 1)
                                ExtractedText2 = pytesseract.image_to_string(Image.fromarray(inputImage), lang="kaz")
                                ExtractedText2 = cleanText(ExtractedText2).strip()
                                if ExtractedText2!='':
                                    ExtractedText+=" " + ExtractedText2
                                TotalTakenSoFar+=1

                        if (toRemoveNewLine=='1'):
                            ExtractedText = removeNewLineJunky(ExtractedText)
                            
                        verseObject = {
                            'type': soV,
                            'text': ExtractedText
                        }

                        verseArray.append(verseObject)
            chaptersArray.append(verseArray)
            perc = (TotalTakenSoFar*100)/(len(OriginalNameList)-1)
            if perc > 100:
                perc = 100
            print  "Progress: " + str(perc) + "%..."

        #save json
        print  "Progress: 100%"
        saveAsJson = bookRoot + "/output.json"
        chaptersArray.pop(0) #burst off the top empty one
        jsonData = {'book_title' : bookName, 'chapters': chaptersArray}
        with codecs.open(saveAsJson, 'w', encoding='utf-8') as outfile:
            json.dump(jsonData, outfile, ensure_ascii=False, indent = 4)
        print "JSon saved as: " + saveAsJson

    #kill oCV
    cv2.waitKey(0)    

def removeNewLineJunky(inputString):
    inputString = ' '.join([line.strip() for line in inputString.strip().splitlines()])
    inputString = inputString.replace("-\\r\\n", "")
    inputString = inputString.replace("-\\n", "")
    inputString = inputString.replace("\\n", " ")
    inputString = inputString.replace("-\r\n", "")
    inputString = inputString.replace("-\n", "")
    inputString = inputString.replace("\n", " ")
    while (inputString.count("  ")>0):
        inputString = inputString.replace("  ", " ")
    return inputString


################################## main procedure #########################################################
if __name__ == '__main__':
    # open files in folder
    # directoryPath = 'F:\Upwork_Working\German_OCR\BookOCR\BookOCR\images'
    # isRemoveNewLine = True

    directoryPath = sys.argv[1]
    removeNewLine = sys.argv[2]

    if directoryPath.strip()=='':
        print "Directory location isn't given!"
        sys.exit(0) 

    if os.path.exists(directoryPath)==False:
        print "Invalid Directory"
        sys.exit(0)

    processBooks(directoryPath, removeNewLine)